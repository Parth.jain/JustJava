/**
 * IMPORTANT: Make sure you are using the correct package name. 
 * This example uses the package name:
 * package com.example.android.justjava
 * If you get an error when copying this code into Android studio, update it to match teh package name found
 * in the project's AndroidManifest.xml file.
 **/

package com.example.android.justjava;



import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URI;
import java.text.NumberFormat;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {
    int quantity=1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {

        //for EditText
        EditText namefield=(EditText)findViewById(R.id.name_field);
        String name=namefield.getText().toString();
        Log.v("MainActivity","Entered name: "+name);

        //CheckBox Values passing
        CheckBox whippedCreamCheckBox=(CheckBox)findViewById(R.id.whipped_cream_checkbox);
        boolean hasWhippedCream=whippedCreamCheckBox.isChecked();
        Log.v("MainActivity","Has Whipped Cream: "+hasWhippedCream);
        CheckBox chocolateCheck=(CheckBox) findViewById(R.id.chocolate_checkbox);
        boolean hasChocolate=chocolateCheck.isChecked();
        Log.v("MainActivity","Chocolate Checkbox: "+hasChocolate);

        //Calculation of Price
        int price= calculatePrice(hasWhippedCream,hasChocolate);
        String priceMessage=createOrderSummary(name,price,hasWhippedCream,hasChocolate);

            //displayMessage(priceMessage);

            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:")); // only email apps should handle this
            intent.putExtra(Intent.EXTRA_SUBJECT, "Just Java Intent for "+name);
            intent.putExtra(Intent.EXTRA_TEXT,priceMessage);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }



        //displayMessage(priceMessage);
       }

    private String createOrderSummary(String Name, int price,boolean hasWhippedCream,boolean addChocolate) {
       String priceMessage="Name:"+Name+"\nQuantity:"+quantity;
       priceMessage=priceMessage+"\nAdd Whipped Cream: "+hasWhippedCream;
       priceMessage=priceMessage+"\nAdd Chocolate?  "+addChocolate;
       priceMessage=priceMessage+"\nTotal: $"+price;
       priceMessage=priceMessage+"\nThank you!";
       return priceMessage;
    }

    private int calculatePrice(boolean addWhippedCream,boolean addChocolate) {
        int basePrice=5;
        if(addWhippedCream==true){
        basePrice=basePrice+1;
        }
        if(addChocolate=true)
        {
        basePrice=basePrice+2;
        }
        return basePrice*quantity;
    }

    public void increment(View view){
        if(quantity==100){
            Toast.makeText(this,"you canot have more than 100 coffee",Toast.LENGTH_LONG).show();
            return;
        }
        quantity=quantity+1;
        displayQuantity(quantity);
    }

    public void decrement(View view) {
        if(quantity==1){

            Toast.makeText(this,"you canot have less than 1 coffee",Toast.LENGTH_LONG).show();

            return;
        }
        quantity=quantity-1;
        displayQuantity(quantity);
    }
    /**
     * This method displays the given quantity value on the screen.
     */
    private void displayQuantity(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText(""+number);

    }
    /**
     * This method displays the given price on the screen.
     */


   /* public void displayMessage(String message){
        TextView orderSummaryTextView = (TextView) findViewById(R.id.order_summary_text_view);
        orderSummaryTextView.setText(message);
        }*/

    }
